# Gamer chat

Built on [react-webpack-babel](https://github.com/alicoding/react-webpack-babel), using [React](https://facebook.github.io/react/), [Alt](https://alt.js.org), [Webpack](http://webpack.github.io/) and [Babel](https://babeljs.io/).

Initial setup: `npm install`

### To run

You can simply run webpack build using this command: 

```
> $ npm run build
```

If you want to run with webpack-dev-server simply run this command: 

```
> $ npm run dev
```

### To test

jasmine-es6 runs a test from the /specs folder:

```
 > $ npm test
```

### To deploy

Run a production build (optimizations for React, etc)

```
> $ npm run deploy
```

Commit updated assets in the dist folder

```
> $ git add dist && git commit -m <your message here>
```

Push updated dist to heroku / dokku

```
> $ git subtree push --prefix dist <yourserver> master
```
