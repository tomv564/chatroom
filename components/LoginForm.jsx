/*eslint jsx-quotes: [2, "prefer-double"] */
/*eslint space-before-function-paren: [1, "never"] */

import React from 'react';
import {UserActions} from '../actions';
import {UserStore} from '../stores';

class LoginBox extends React.Component {
  constructor(props) {
    super(props);
    let initialState = UserStore.getState();
    initialState.username = '';
    this.state = initialState;
    this.onUsersChanged = this.onUsersChanged.bind(this);
  }
  componentDidMount() {
    React.findDOMNode(this.refs.inputbox).focus();
    UserStore.listen(this.onUsersChanged);
  }
  componentWillUnmount() {
    UserStore.unlisten(this.onUsersChanged);
  }
  onUsersChanged(state) {
    this.setState(state);
  }
  render() {
    return (
        <div className="form-group">
          {this.state.warning ? <p className="alert alert-danger">{this.state.warning}</p> : ''}
          <label htmlFor="username">Nickname</label>
          <input ref="inputbox" id="username" name="username" type="text" maxLength="20" value={this.state.username} onChange={this.onChange.bind(this)} onKeyDown={this.onKeyDown.bind(this)} className="form-control" placeholder="Enter a nickname..."/>
        </div>
      );
  }
  onChange(e) {
    this.setState({username: e.target.value});
  }
  onKeyDown(e) {
    if (e.keyCode === 13 && this.state.username.length > 0) {
      let cleanName = this.state.username.trim();
      if (this.state.users.some(u => u.name.toLowerCase() === cleanName.toLowerCase())) {
        this.setState({warning: 'This name is already taken!'});
      } else {
        UserActions.addCurrentUser({name: this.state.username, status: 'online'});
        this.setState({username: '', warning: ''});
      }
    }
  }
}

export class LoginForm extends React.Component {
  render() {
    return (
      <div className="loginform form-inline">
        <p>Please choose a name to join.</p>
        <LoginBox/>
      </div>
      );
  }
}
