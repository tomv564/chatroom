/*eslint jsx-quotes: [2, "prefer-double"] */
/*eslint space-before-function-paren: [1, "never"] */

import React from 'react';
import {MessageStore} from '../stores';

export class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = MessageStore.getState();
    this.onChange = state => this.setState(state);
  }
  componentDidMount() {
    MessageStore.listen(this.onChange);
  }
  componentWillUnmount() {
    MessageStore.unlisten(this.onChange);
  }
  componentDidUpdate() {
    let _this = this;
    // scroll into view after painting.
    window.requestAnimationFrame(() => {
      let node = React.findDOMNode(_this);
      if (node !== undefined) {
        node.scrollTop = node.scrollHeight;
      }
    });
  }
  render() {
    let items = this.state.messages.map(message =>
      <MessageDisplay key={message.id} name={message.name} text={message.text}/>
      );
    return (
      <div className="scrolling-container">
        <ul className="messagelist list-unstyled">
          {items}
        </ul>
      </div>
      );
  }
}

MessageList.propTypes = { messages: React.PropTypes.arrayOf(React.PropTypes.object) };

class MessageDisplay extends React.Component {
  render() {
    return (
      <li key={this.props.id}>
        <span className="message-name">{this.props.name}</span>
        <span className="message-text">{this.props.text}</span>
      </li>
      );
  }
}

MessageDisplay.propTypes = {
  id: React.PropTypes.number,
  name: React.PropTypes.string,
  text: React.PropTypes.string
};
