/*eslint jsx-quotes: [2, "prefer-double"] */
/*eslint space-before-function-paren: [1, "never"] */

import React from 'react';
import {MessageActions} from '../actions';

export class InputBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }
  componentDidMount() {
    React.findDOMNode(this.refs.inputbox).focus();
  }
  render() {
    return (
        <input ref="inputbox" type="text" maxLength="200" value={this.state.text} onChange={this.onChange.bind(this)} onKeyDown={this.onKeyDown.bind(this)} className="form-control" placeholder="Enter a message..."/>
      );
  }
  onChange(e) {
    this.setState({text: e.target.value});
  }
  onKeyDown(e) {
    if (e.keyCode === 13 && this.state.text.length > 0) {
      MessageActions.addMessage({name: this.props.currentUser.name, text: this.state.text});
      this.setState({text: ''});
    }
  }
}

InputBox.propTypes = {
  currentUser: React.PropTypes.object
};
