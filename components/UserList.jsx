/*eslint jsx-quotes: [2, "prefer-double"] */
/*eslint space-before-function-paren: [1, "never"] */

import React from 'react';
import {UserStore} from '../stores';
import {compareUser} from '../util';

export class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = UserStore.getState();
    this.onChange = state => this.setState(state);
  }
  componentDidMount() {
    UserStore.listen(this.onChange);
  }
  componenntWillUnmount() {
    console.log('unmounting userlist');
    UserStore.unlisten(this.onChange);
  }
  render() {
    let sorted = this.state.users.sort(compareUser);
    let items = sorted.map(user =>
       <li key={user.id}><UserDisplay name={user.name} status={user.status} statusDetail={user.statusDetail}/></li>
      );
    return (
      <div>
        <ul className="userlist list-unstyled">
          {items}
        </ul>
        <div className="instructions">
          <p>The bots respond in two ways:</p>
          <ul><li>By name (eg. <i>'Hi Luke!'</i>)</li>
          <li>By 'play' + their game (eg. <i>'play Halo'</i>)</li>
          </ul>
          <p>The bots only play 'their' game, and will not respond while away or busy.</p>
        </div>
      </div>
      );
  }
}

class UserDisplay extends React.Component {
  render() {
    let status = 'statusindicator-' + this.props.status;
    let classes = 'glyphicon glyphicon-user statusindicator ' + status;
    return (
      <div className="userdisplay">
        <span className={classes}></span>
          <p className="username"><span>{this.props.name}</span><br/>
          <span className="statusDetail">{this.statusDetail()}</span></p>
      </div>
      );
  }
  statusDetail() {
    return this.props.statusDetail || this.props.status;
  }
}

UserDisplay.propTypes = {
  status: React.PropTypes.string,
  statusDetail: React.PropTypes.string,
  name: React.PropTypes.string
};
