/* eslint-env jasmine */
/* eslint padded-blocks: 0 */

import * as util from '../util';

describe('util', function () {

  it('sorts users by status and name', function () {

    let users = [
      {name: 'Luke', status: 'away'},
      {name: 'asdf', status: 'away'},
      {name: 'BBB', status: 'busy'},
      {name: 'Zaphod Beeblebrox', status: 'online'}
    ];

    users.sort(util.compareUser);

    expect(users[0].name).toBe('Zaphod Beeblebrox');
    expect(users[1].name).toBe('BBB');
    expect(users[2].name).toBe('asdf');
    expect(users[3].name).toBe('Luke');

  });

});

