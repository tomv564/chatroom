/*eslint space-before-function-paren: [2, "never"] */

import alt from './alt';

class MessageActionsImpl {
  addMessage(message) {
    this.dispatch(message);
  }
}
class UserActionsImpl {
  addUser(user) {
    this.dispatch(user);
  }
  addCurrentUser(user) {
    this.dispatch(user);
  }
  setStatus(user, status, statusDetail) {
    this.dispatch({user: user, status: status, statusDetail: statusDetail});
  }
}

export const MessageActions = alt.createActions(MessageActionsImpl);
export const UserActions = alt.createActions(UserActionsImpl);
