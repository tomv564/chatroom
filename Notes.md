# Notes

## Initial Setup

Started with a simple react + webpack starter kit, cloned + ran npm install
Created Sublime project, Stole Tern config from another project
Ignored node_modules

Created bitbucket account and initial commit

## Document structure setup

Cleaned up the built-in jumbotron, restrain app width.

## View components

Do we need a flux impl? Need an eventemitter
Redux: single -store approach is tempting to try but the Provider setup is another new thing to learn. Use Alt because familiar and ES6 friendly.

### User list

* Must show user and status. DONE
* Must be sorted by status and name. DONE
* Connected to a store. DONE

### Message list

* Must show user and text DONE
* Must listen to store updates DONE
* Should be scrollable DONE
* Scroll to latest messages DONE
* Highlight newly arrived messages TODO

### Input box

* Must submit message on enter key if text not blank DONE
* Must clear box after submitting message DONE
* Must receive focus on page load DONE

## Styling and refactoring

Make things look 'nice'

* Move CSS into seperate file - DONE
* Enable a bootstrap theme?
* Boxes / lines / backgrounds - DONE

Make the code organized:

* Move code into component files, stores, actions, util? DONE


## User functionality

* I can choose my nickname before I join DONE
* I see unread messages in the title bar ?

## Bot functionality

* Presence changes every once in a while DONE (lurker bot)
* Speaks once in a while DONE (halo bot)
* Replies when addressed

Bots are loaded and signed in at app startup. Future option is to include signon/signoff behaviour for the bots.

**Prescence Behaviour**:

Use a recursive setTimeout loop with random pauses.

* go 'online'
* check unread messages of interest
* return to 'away' after a while

API requirements

* UserActions.setStatus


**Intents**:

* wants to start a game

  * Go to online
  * Posts to start game
  * If another user response saying they want to play it:
  * Go to busy
  * Return to online
  * Post something about the game
  * Go to Away after delay.

* wants to join a game
  * If another user offers to start a game that the bot has, respond affirmatively.
  * Go to busy
  * Go to Away after a delay.

API requirements

 * Listen to MessageStore
 * MessageActions.addMessage
 * UserActions.setStatus


### Testing

* Input limits (input maxlength, react auto-escapes input when rendering) DONE
* Cannot register an existing name DONE
* Works on IE
* Works on Firefox
* Works on Chrome
* Works on safari mobile
* Works on android mobile









