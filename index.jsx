/*eslint jsx-quotes: [2, "prefer-double"] */
/*eslint space-before-function-paren: [1, "never"] */

require('./node_modules/bootstrap/dist/css/bootstrap.min.css');
require('./public/main.css');
import React from 'react';
import {UserActions} from './actions';
import {UserStore} from './stores';
import {MessageList} from './components/MessageList';
import {InputBox} from './components/InputBox';
import {LoginForm} from './components/LoginForm';
import {UserList} from './components/UserList';
import {gamerBot} from './bots';

let luke = {name: 'Luke', status: 'online'};
gamerBot(luke, 'Starwars Battlefront');
UserActions.addUser(luke);

let daniel = {name: 'Daniel', status: 'online'};
gamerBot(daniel, 'Battlefield');
UserActions.addUser(daniel);

let masterChief = {name: 'Master Chief', status: 'online'};
gamerBot(masterChief, 'Halo');
UserActions.addUser(masterChief);

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {currentUser: null};
    this.onChange = this.onChange.bind(this);
  }
  componentDidMount() {
    UserStore.listen(this.onChange);
  }
  componentWillUnmount() {
    UserStore.unlisten(this.onChange);
  }
  onChange(newState) {
    if (this.state.currentUser !== newState.currentUser) {
      this.setState({currentUser: newState.currentUser});
    }
  }
	render() {
		return this.state.currentUser ? (
      <div className="row">
        <div className="col-sm-8">
          <MessageList/>
          <InputBox currentUser={this.state.currentUser}/>
        </div>
        <div className="col-sm-4">
          <UserList/>
        </div>
      </div>
		) : (
      <div className="row">
        <div className="col-sm-12">
          <LoginForm/>
        </div>
      </div>
    );
	}
}

React.render(<App/>, document.querySelector('#myApp'));
