/*eslint space-before-function-paren: [2, "never"] */

import {UserActions, MessageActions} from './actions';
import {MessageStore} from './stores';

function randomDelay() {
  return Math.random() * 30000;
}

function shortDelay() {
  return Math.random() * 5000;
}

function playGameDelay() {
  return 20000 + Math.random() * 10000;
}

export function gamerBot(user, game) {
  const addressedRegexp = new RegExp(`.*${user.name}.*`, 'i');
  const playRequestRegexp = new RegExp(`.*play.*${game}.*`, 'i');
  const announcements = [`If you want to play ${game}, just say so!`,
                        `I'm so bored, let's play ${game}!`,
                        `Anyone up for a game for ${game}?`];

  let readUpTo = 0;
  let isAway = false;
  let isBusy = false;
  let playingWith = '';
  let messages = [];

  function randomPick(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
  }

  function announceIntent() {
    say(randomPick(announcements));
  }

  function toggleStatus() {
    if (!isBusy) {
      isAway = !isAway;
      let newStatus = isAway ? 'away' : 'online';
      UserActions.setStatus(user, newStatus);
    }

    setTimeout(toggleStatus, randomDelay());

    if (!isAway && !isBusy) {
      checkUnreadMessages();
    }
  }

  function say(text) {
    MessageActions.addMessage.defer({name: user.name, text: text});
  }

  function startGameWith(buddy) {
    playingWith = buddy;
    say(`Yes sir! Starting a game of ${game} right now!`);
    UserActions.setStatus(user, 'busy', `playing ${game}`);
    isBusy = true;
    setTimeout(returnFromGame, playGameDelay());
  }

  function returnFromGame() {
    UserActions.setStatus(user, 'online');
    say(`Good game ${playingWith}!`);
    isBusy = false;
    playingWith = '';
  }

  function checkUnreadMessages() {
    let unread = messages.slice(readUpTo).filter(msg => msg.name !== user.name);
    let messageCount = messages.length;
    readUpTo = messageCount;

    let playRequest = unread.find(msg => playRequestRegexp.test(msg.text));
    if (playRequest) {
      startGameWith(playRequest.name);
      return; // don't respond to more than one request at a time.
    }

    let question = unread.find(msg => addressedRegexp.test(msg.text));
    if (question) {
      say(`I have no idea what you're talking about, ${question.name}. Want to play a game?`);
    }
  }

  function messagesChanged(state) {
    messages = state.messages;

    if (!isAway && !isBusy) {
      setTimeout(checkUnreadMessages, shortDelay());
    }
  }

  setTimeout(announceIntent, shortDelay());
  setTimeout(toggleStatus, randomDelay());
  MessageStore.listen(state => messagesChanged(state));
}
