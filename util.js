/*eslint space-before-function-paren: [2, "never"] */

function compareUsername(name1, name2) {
  if (name1 > name2) {
    return 1;
  }
  if (name1 < name2) {
    return -1;
  }
  return 0;
}

const orderedStatuses = ['away', 'busy', 'online'];

function compareStatus(u1, u2) {
  let u1Status = orderedStatuses.indexOf(u1.status);
  let u2Status = orderedStatuses.indexOf(u2.status);

  if (u1Status > u2Status) {
    return -1;
  }
  if (u1Status < u2Status) {
    return 1;
  }

  return 0;
}

export function compareUser(u1, u2) {
  if (u1.status === u2.status) {
    return compareUsername(u1.name.toLowerCase(), u2.name.toLowerCase());
  } else {
    return compareStatus(u1, u2);
  }
}
