/*eslint space-before-function-paren: [1, "never"] */

import alt from './alt';
import {MessageActions, UserActions} from './actions';

class MessageStoreImpl {
  constructor() {
    this.messages = [];

    this.bindListeners({
      handleAddMessage: MessageActions.ADD_MESSAGE
    });
  }

  handleAddMessage(message) {
    message.id = this.messages.length + 1;
    this.messages.push(message);
  }
}

class UserStoreImpl {
  constructor() {
    this.users = [];
    this.currentUser = null;

    this.bindListeners({
      handleAddUser: UserActions.ADD_USER,
      handleSetStatus: UserActions.SET_STATUS,
      handleAddCurrentUser: UserActions.ADD_CURRENT_USER
    });
  }

  handleAddUser(user) {
    user.id = this.users.length + 1;
    this.users.push(user);
  }

  handleAddCurrentUser(user) {
    this.handleAddUser(user);
    this.currentUser = user;
  }

  handleSetStatus(statusChange) {
    let {user, status, statusDetail} = statusChange;
    let foundUser = this.users.find((u) => u.id === user.id);
    if (foundUser) {
      foundUser.statusDetail = statusDetail;
      foundUser.status = status;
    }
  }

}

export const MessageStore = alt.createStore(MessageStoreImpl, 'MessageStore');
export const UserStore = alt.createStore(UserStoreImpl, 'UserStore');
